package casino.idfactory;

/**
 * Factory for creation of GeneralID objects.
 * creation of the right object is done by specifying the type to create
 * the specified type is case insensitive
 *
 * when the type is not present, null is returned.
 */
public class IDFactory {

    public GeneralID getID(ID idType) {
        if (idType == ID.CARD) {
            return new CardID();
        } else if (idType == ID.BETTING_ROUND) {
            return new BettingRoundID();
        } else if (idType == ID.BET) {
            return new BetID();
        } else if (idType == ID.GAMING_MACHINE) {
            return new GamingMachineID();
        }

        return null;
    }
}
