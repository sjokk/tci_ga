package casino.idfactory;

public enum ID {
    CARD,
    BET,
    BETTING_ROUND,
    GAMING_MACHINE
}
