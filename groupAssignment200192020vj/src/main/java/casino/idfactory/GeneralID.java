package casino.idfactory;

import java.sql.Timestamp;
import java.util.UUID;

abstract public class GeneralID implements Comparable {
    private UUID uniqueID;
    private Timestamp timestamp;

    public GeneralID() {
        uniqueID = UUID.randomUUID();
        timestamp = new Timestamp(System.currentTimeMillis());
    }

    public UUID getUniqueID() {
        return uniqueID;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    @Override
    public int compareTo(Object that) {
        int BEFORE = -1;
        int AFTER = 1;

        if (that == null || !(that instanceof GeneralID)) throw new IllegalArgumentException();

        GeneralID thatID = (GeneralID)that;

        if (this.getTimestamp().compareTo(thatID.getTimestamp()) < 0) {
            return BEFORE;
        } else if (this.getTimestamp().compareTo(thatID.getTimestamp()) > 0) {
            return AFTER;
        }

        return getUniqueID().compareTo(thatID.getUniqueID());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return false;
    }
}
