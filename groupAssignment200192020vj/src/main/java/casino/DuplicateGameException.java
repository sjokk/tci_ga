package casino;

public class DuplicateGameException extends Exception {
    public DuplicateGameException(String message) {
        super(message);
    }
}
