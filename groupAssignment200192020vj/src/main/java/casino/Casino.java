package casino;

import casino.bet.Bet;
import casino.cashier.BetNotExceptedException;
import casino.cashier.ICashier;
import casino.cashier.IPlayerCard;
import casino.game.IGame;

import java.util.HashMap;

public class Casino implements ICasino {
    private HashMap<String, IGame> games = new HashMap<>();
    private ICashier cashier;

    public Casino() {
        
    }

    public Casino(ICashier cashier) {
        this.cashier = cashier;
    }

    @Override
    public void addGame(String gameName, IGame gameToAdd) throws DuplicateGameException {
        if (gameName == "" || gameName == null) {
            throw new IllegalArgumentException("Game name can not be an empty string or null!");
        }

        if (gameToAdd == null) {
            throw new IllegalArgumentException("Game can not be null!");
        }

        if (getGame(gameName) != null) {
            throw new DuplicateGameException("Game already exists!");
        }

        games.put(gameName, gameToAdd);
    }

    @Override
    public IGame getGame(String name) {
        return games.get(name);
    }

    @Override
    public boolean checkIfBetIsValid(IPlayerCard card, Bet betToCheck) throws BetNotExceptedException {
        if (card == null) {
            throw new IllegalArgumentException("Player card can not be null!");
        }

        if (betToCheck == null) {
            throw new IllegalArgumentException("Bet can not be null!");
        }

        return cashier.checkIfBetIsValid(card, betToCheck);
    }
}
