package casino.game;

import bettingauthoritiyAPI.BetToken;
import bettingauthoritiyAPI.BettingAuthority;
import casino.bet.Bet;
import casino.bet.BetResult;
import casino.gamingmachine.GamingMachine;
import casino.gamingmachine.IGamingMachine;
import casino.idfactory.BettingRoundID;
import casino.idfactory.ID;
import casino.idfactory.IDFactory;

import java.util.Set;

public class Game extends AbstractGame {


    public Game(IDFactory idFactory, GameRule gameRule, BettingAuthority bettingAuthority) {
        super(idFactory, gameRule, bettingAuthority);
    }

    /**
     * create and start a new BettingRound.
     * when called when a current bettinground is active: the current bettinground ends and a new
     * bettinground is created, which becomes the current bettinground.
     */
    @Override
    public void startBettingRound() {
        super.startBettingRound();
    }


    /**
     * Accept a bet on the current betting round.
     * <p>
     * log relevant information for the betloggingauthority.
     *
     * @param bet           the bet to be made on the betting round
     * @param gamingMachine gamingmachine which places bet on this game.
     * @return true when bet is accepted by the game, otherwise false.
     * @throws NoCurrentRoundException when no BettingRound is currently active.
     */
    @Override
    public boolean acceptBet(Bet bet, IGamingMachine gamingMachine) throws NoCurrentRoundException {
        boolean betPlaced = super.acceptBet(bet, gamingMachine);

        if (!betPlaced) {
            return false;
        }

        gamingMachines.add(gamingMachine);
        betLoggingAuthority.addAcceptedBet(bet, bettingRound.getBettingRoundID(), gamingMachine.getGamingMachineID());
        return true;
    }

    /**
     * Calculate the winner using the gamerules.
     * Let the gamingMachine update the winner's amount at the bank teller
     * <p>
     * log relevant information for the betloggingauthority.
     */
    @Override
    public void determineWinner() {
        super.determineWinner();
    }

    /**
     * determine if the right number of bets are done (determined by gamerules) to be able to
     * calculate a winner.
     *
     * @return true if all necessary bets are made in the betting round, otherwise false
     */
    @Override
    public boolean isBettingRoundFinished() {
        if (bettingRound.numberOFBetsMade() < gameRule.getMaxBetsPerRound()) {
            return false;
        }
        return true;
    }
}
