package casino.game;

import bettingauthoritiyAPI.BetToken;
import casino.bet.Bet;
import casino.idfactory.BettingRoundID;

import java.util.HashSet;
import java.util.Set;

public class BettingRound implements IBettingRound{
    private BetToken betToken;
    private BettingRoundID bettingRoundID;
    private Set<Bet> bets;

    public BettingRound(BetToken betToken, BettingRoundID bettingRoundID) {
        if(betToken == null || bettingRoundID == null){throw new IllegalArgumentException();}
        this.betToken = betToken;
        this.bettingRoundID = bettingRoundID;
        this.bets = new HashSet<Bet>();
    }


    @Override
    public BettingRoundID getBettingRoundID() {
        return bettingRoundID;
    }

    @Override
    public boolean placeBet(Bet bet) {
        if(bet == null){throw new IllegalArgumentException();}
        if(!bets.contains(bet)){
            for (Bet b:bets) {
                if(b.getPlayerCard() == bet.getPlayerCard()){
                    return false;
                }
            }
            bets.add(bet);
            return true;
        }
        return false;
    }

    @Override
    public Set<Bet> getAllBetsMade() {
        return bets;
    }

    @Override
    public BetToken getBetToken() {
        return betToken;
    }

    @Override
    public int numberOFBetsMade() {
        return bets.size();
    }
}
