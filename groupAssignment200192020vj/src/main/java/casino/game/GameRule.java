package casino.game;

import casino.bet.Bet;
import casino.bet.BetResult;
import casino.bet.MoneyAmount;

import java.util.Set;

public class GameRule implements IGameRule {
    private int maxBetsPerRound;

    public GameRule(int maxBetsPerRound) {
        if(maxBetsPerRound <= 1){throw new IllegalArgumentException();}
        this.maxBetsPerRound = maxBetsPerRound;
    }

    @Override
    public BetResult determineWinner(Integer randomWinValue, Set<Bet> bets) {
        if(randomWinValue == null || bets == null){throw new IllegalArgumentException();}
        if(bets.size() > 0 && bets.size() <= maxBetsPerRound){
            int remainder = Math.abs(randomWinValue % bets.size());
            Bet[] betsArray = bets.toArray(new Bet[bets.size()]);
            Bet winningBet = betsArray[remainder];
            long winningAmount = 0;
            for (Bet b: betsArray) {
                winningAmount += b.getMoneyAmount().getAmountInCents();
            }
            MoneyAmount winningMoneyAmount = new MoneyAmount(winningAmount);
            BetResult betResult = new BetResult(winningBet, winningMoneyAmount);
            return  betResult;
        }
        return null;
    }

    @Override
    public int getMaxBetsPerRound() {
        return maxBetsPerRound;
    }
}
