package casino.game;

import bettingauthoritiyAPI.*;
import casino.bet.Bet;
import casino.bet.BetResult;
import casino.gamingmachine.GamingMachine;
import casino.gamingmachine.IGamingMachine;
import casino.idfactory.BettingRoundID;
import casino.idfactory.ID;
import casino.idfactory.IDFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public abstract class AbstractGame implements IGame {

    protected final IDFactory idFacory;
    protected final GameRule gameRule;
    protected final BettingAuthority bettingAuthority;
    protected final IBetTokenAuthority betTokenAuthority;
    protected final IBetLoggingAuthority betLoggingAuthority;
    protected Set<IGamingMachine> gamingMachines = new HashSet<IGamingMachine>();

    protected BettingRound bettingRound = null;

    protected boolean roundActive = false;

    public AbstractGame(IDFactory idFactory, GameRule gameRule, BettingAuthority bettingAuthority) {

        if (idFactory == null || gameRule == null || bettingAuthority == null) {
            throw new IllegalArgumentException("idFactory, gameRule, and bettingAuthority cannot be null");
        }

        this.idFacory = idFactory;
        this.gameRule = gameRule;
        this.bettingAuthority = bettingAuthority;
        this.betTokenAuthority = bettingAuthority.getTokenAuthority();
        this.betLoggingAuthority = bettingAuthority.getLoggingAuthority();
    }

    /**
     * create and start a new BettingRound.
     * when called when a current bettinground is active: the current bettinground ends and a new
     * bettinground is created, which becomes the current bettinground.
     */
    public void startBettingRound() {
        if (!roundActive) {
            startRound();
        } else if (!isBettingRoundFinished()) {
            throw new RoundNotFinishedException("The round has not yet finished, more bets need to be made");
        } else if (roundActive && isBettingRoundFinished()) {
            determineWinner();
            startRound();
        }
    }

    private void startRound() {
        BettingRoundID bettingRoundID = (BettingRoundID) idFacory.getID(ID.BETTING_ROUND);
        BetToken betToken = betTokenAuthority.getBetToken(bettingRoundID);
        BettingRound bettingRound = new BettingRound(betToken, bettingRoundID);

        this.bettingRound = bettingRound;
        roundActive = true;
        betLoggingAuthority.startBettingRound(bettingRound);
    }

    /**
     * Accept a bet on the current betting round.
     * <p>
     * log relevant information for the betloggingauthority.
     *
     * @param bet           the bet to be made on the betting round
     * @param gamingMachine gamingmachine which places bet on this game.
     * @return true when bet is accepted by the game, otherwise false.
     * @throws NoCurrentRoundException when no BettingRound is currently active.
     */
    @Override
    public boolean acceptBet(Bet bet, IGamingMachine gamingMachine) throws NoCurrentRoundException {
        if (bet == null || gamingMachine == null) {
            throw new IllegalArgumentException("Bet and gamingMachine cannot be null");
        }

        if (!roundActive) {
            throw new NoCurrentRoundException("Bet cannot be placed, there is no active round");
        }

        return bettingRound.placeBet(bet);
    }

    /**
     * Calculate the winner using the gamerules.
     * Let the gamingMachine update the winner's amount at the bank teller
     * <p>
     * log relevant information for the betloggingauthority.
     */
    @Override
    public void determineWinner() {
        if (!bettingRound.getAllBetsMade().isEmpty()) {
            int randomWinValue = betTokenAuthority.getRandomWinValue();
            Set<Bet> bets = bettingRound.getAllBetsMade();
            BetResult betResult = gameRule.determineWinner(randomWinValue, bets);
            for (IGamingMachine gamingMachine : gamingMachines) {
                gamingMachine.acceptWinner(betResult);
            }
            roundActive = false;
            betLoggingAuthority.endBettingRound(bettingRound, betResult);
        }
    }
}

