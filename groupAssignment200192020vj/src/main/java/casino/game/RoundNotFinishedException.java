package casino.game;

public class RoundNotFinishedException extends RuntimeException {
    public RoundNotFinishedException(String message) {
        super(message);
    }
}
