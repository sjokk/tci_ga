package casino.game;

public class NoCurrentRoundException extends RuntimeException {
    public NoCurrentRoundException(String message) {
        super(message);
    }
}
