package casino.bet;

import casino.cashier.IPlayerCard;
import casino.idfactory.BetID;

/**
 * immutable class.
 * keeps unique betID and moneyamount in the bet.
 */
public class Bet {
    private BetID betID;
    private MoneyAmount moneyAmount;
    private IPlayerCard playerCard;

    public Bet(BetID betID, MoneyAmount moneyAmount, IPlayerCard playerCard) {
        if(betID == null || moneyAmount == null || playerCard == null){
            throw new IllegalArgumentException();
        }
        this.betID = betID;
        this.moneyAmount = moneyAmount;
        this.playerCard = playerCard;
    }

    public BetID getBetID() {
        return betID;
    }

    public MoneyAmount getMoneyAmount() {
        return moneyAmount;
    }

    public IPlayerCard getPlayerCard() {
        return playerCard;
    }
}
