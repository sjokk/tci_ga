package casino.gamingmachine;

import casino.bet.Bet;
import casino.bet.BetResult;
import casino.bet.MoneyAmount;
import casino.cashier.BetNotExceptedException;
import casino.cashier.ICashier;
import casino.cashier.IPlayerCard;
import casino.game.IGame;
import casino.idfactory.BetID;
import casino.idfactory.GamingMachineID;
import casino.idfactory.ID;
import casino.idfactory.IDFactory;

public class GamingMachine implements IGamingMachine, ISubject {
    private IObserver obs;

    private IDFactory idFactory;
    private IPlayerCard playerCard;
    private GamingMachineID id;
    private ICashier cashier;
    private Bet madeBet;
    private IGame game;

    public GamingMachine() {
        idFactory = new IDFactory();
    }

    public GamingMachine(ICashier cashier) {
        this();
        this.cashier = cashier;
    }

    public GamingMachine(ICashier cashier, IGame game) {
        this(cashier);
        this.game = game;
    }

    public GamingMachine(GamingMachineID id) {
        this.id = id;
    }

    @Override
    public boolean placeBet(long amountInCents) throws NoPlayerCardException {
        if (getPlayerCard() == null) {
            throw new NoPlayerCardException("No player card connected to this machine!");
        }

        madeBet = new Bet((BetID) idFactory.getID(ID.BET), new MoneyAmount(amountInCents), playerCard);;

        if (obs != null) {
            obs.update(madeBet);
        }

        boolean betPlaced = false;

        try {
            ICashier cashier = getCashier();

            betPlaced = cashier.checkIfBetIsValid(getPlayerCard(), madeBet)
                        && game.acceptBet(madeBet, this);

            if (!betPlaced) {
                madeBet = null;
            }
        } catch (BetNotExceptedException e) {
            e.printStackTrace();
        } finally {
            return betPlaced;
        }
    }

    @Override
    public void acceptWinner(BetResult winResult) {
        if (winResult.getWinningBet().equals(madeBet)) {
            cashier.addAmount(playerCard, winResult.getAmountWon());
        }

        madeBet = null;
    }

    @Override
    public GamingMachineID getGamingMachineID() {
        return id;
    }

    @Override
    public void connectCard(IPlayerCard card) {
        if (card == null) {
            throw new IllegalArgumentException("Player card can not be null!");
        }

        playerCard = card;
    }

    @Override
    public IPlayerCard getPlayerCard() {
        return playerCard;
    }

    public ICashier getCashier() {
        return cashier;
    }

    @Override
    public void attach(IObserver observer) {
        obs = observer;
    }

    public Bet getMadeBet() {
        return madeBet;
    }
}
