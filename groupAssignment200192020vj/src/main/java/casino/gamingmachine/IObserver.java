package casino.gamingmachine;

import casino.bet.Bet;

public interface IObserver {
    void update(Bet lastAttemptedBet);
}
