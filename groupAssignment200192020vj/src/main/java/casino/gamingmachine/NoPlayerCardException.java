package casino.gamingmachine;

public class NoPlayerCardException extends Exception {
    public NoPlayerCardException(String message) {
        super(message);
    }
}
