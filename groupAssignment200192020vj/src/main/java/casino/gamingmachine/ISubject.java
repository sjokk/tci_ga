package casino.gamingmachine;

public interface ISubject {
    void attach(IObserver observer);
}
