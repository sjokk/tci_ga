package casino.cashier;

import bettingauthoritiyAPI.BetLoggingAuthority;
import bettingauthoritiyAPI.IBetLoggingAuthority;
import casino.bet.Bet;
import casino.bet.MoneyAmount;

import java.util.ArrayList;
import java.util.List;

public class Cashier implements ICashier {

    private List<IPlayerCard> playerCards;
    private List<IPlayerCard> distributedPlayerCards;
    private List<MoneyAmount> distributedPlayerCardsAmount;
    private IBetLoggingAuthority betLoggingAuthority;

    public Cashier(List<IPlayerCard> playerCards, IBetLoggingAuthority betLoggingAuthority){
        if(playerCards == null || betLoggingAuthority == null){
            throw new IllegalArgumentException();
        }
        this.playerCards = playerCards;
        this.betLoggingAuthority = betLoggingAuthority;
        this.distributedPlayerCards = new ArrayList<>();
        this.distributedPlayerCardsAmount = new ArrayList<>();
    }

    @Override
    public IPlayerCard distributeGamblerCard() {
        if(playerCards.size() > 0){
            IPlayerCard playerCard = playerCards.get(0);
            distributedPlayerCards.add(playerCard);
            distributedPlayerCardsAmount.add(new MoneyAmount(0));
            playerCards.remove(playerCard);
            betLoggingAuthority.handOutGamblingCard(playerCard.getCardID());
            return playerCard;
        }
        return null;
    }

    @Override
    public void returnGamblerCard(IPlayerCard card) {
        int index = distributedPlayerCards.indexOf(card);
        if(index >= 0){
            distributedPlayerCards.remove(index);
            distributedPlayerCardsAmount.remove(index);
            playerCards.add(card);
            betLoggingAuthority.handInGamblingCard(card.getCardID(), card.returnBetIDsAndClearCard());
        }
        else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public boolean checkIfBetIsValid(IPlayerCard card, Bet betToCheck) throws BetNotExceptedException {
        if(card == null || betToCheck == null){throw  new IllegalArgumentException();}
        int index = distributedPlayerCards.indexOf(card);
        if(index >= 0){
            if(distributedPlayerCardsAmount.get(0).getAmountInCents() >= betToCheck.getMoneyAmount().getAmountInCents()){
                long newAmount = distributedPlayerCardsAmount.get(0).getAmountInCents() - betToCheck.getMoneyAmount().getAmountInCents();
                distributedPlayerCardsAmount.set(index, new MoneyAmount(newAmount));
                return true;
            }
        }
        else {
            throw  new BetNotExceptedException();
        }
        return false;
    }

    @Override
    public void addAmount(IPlayerCard card, MoneyAmount amount) {
        if(card == null || amount == null){throw new IllegalArgumentException();}
        int index = distributedPlayerCards.indexOf(card);
        if(index >= 0){
            long currentAmount = distributedPlayerCardsAmount.get(index).getAmountInCents();
            long addAmount = amount.getAmountInCents();
            long newAmount = currentAmount + addAmount;
            distributedPlayerCardsAmount.set(index, new MoneyAmount(newAmount));
        }
        else {
            throw new IllegalArgumentException();
        }
    }

    public List<IPlayerCard> getPlayerCards() {
        return playerCards;
    }

    public IBetLoggingAuthority getBetLoggingAuthority() {
        return betLoggingAuthority;
    }

    public List<IPlayerCard> getDistributedPlayerCards() {
        return distributedPlayerCards;
    }

    public List<MoneyAmount> getDistributedPlayerCardsAmount() {
        return distributedPlayerCardsAmount;
    }
}
