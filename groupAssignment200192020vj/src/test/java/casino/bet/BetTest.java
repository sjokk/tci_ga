package casino.bet;

import casino.cashier.IPlayerCard;
import casino.idfactory.BetID;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.mockito.Mockito.mock;
import static org.fest.assertions.Assertions.assertThat;

public class BetTest {

    private BetID validBetID = mock(BetID.class);
    private MoneyAmount validMoneyAmount = mock(MoneyAmount.class);
    private IPlayerCard validPlayerCard = mock(IPlayerCard.class);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void CreateBet(){
        //arrange
        Bet bet = new Bet(validBetID, validMoneyAmount, validPlayerCard);
        //act
        //assert
        assertThat(bet.getBetID()).isEqualTo(validBetID);
        assertThat(bet.getMoneyAmount()).isEqualTo(validMoneyAmount);
        assertThat(bet.getPlayerCard()).isEqualTo(validPlayerCard);
    }
    @Test
    public void CreateBetWhereBetIDIsNullShouldThrowIllegalArgumentException(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        Bet bet = new Bet(null, validMoneyAmount, validPlayerCard);
    }
    @Test
    public void CreateBetWhereMoneyAmountIsNullShouldThrowIllegalArgumentException(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        Bet bet = new Bet(validBetID, null, validPlayerCard);
    }
    @Test
    public void CreateBetWherePlayerCardIsNullShouldThrowIllegalArgumentException(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        Bet bet = new Bet(validBetID, validMoneyAmount, null);
    }
}