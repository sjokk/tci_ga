package casino.game;

import casino.bet.Bet;
import casino.bet.BetResult;
import casino.bet.MoneyAmount;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.HashSet;
import java.util.Set;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class GameRuleTest {

    int maxBets = 2;
    int randomWinValue = 1234;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void CreateGameRule(){
        //arrange
        GameRule gameRule = new GameRule(maxBets);
        //act
        //assert
        assertThat(gameRule.getMaxBetsPerRound()).isEqualTo(maxBets);
    }
    @Test
    public void CreateGameRuleWithMaxBetsLowerThan1ShouldThrowIllegalArgumentException(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        GameRule gameRule = new GameRule(0);
    }
    @Test
    public void DetermineWinnerWhenNumberOfBetsIsGreaterThan0AndLowerOrEqualToMaxBetsShouldReturnBetResult(){
        //arrange
        GameRule gameRule = new GameRule(maxBets);
        MoneyAmount moneyAmount = mock(MoneyAmount.class);
        when(moneyAmount.getAmountInCents()).thenReturn((long)100);
        Bet bet = mock(Bet.class);
        when(bet.getMoneyAmount()).thenReturn(moneyAmount);
        Bet bet2 = mock(Bet.class);
        when(bet2.getMoneyAmount()).thenReturn(moneyAmount);
        Set<Bet> betSet = new HashSet<Bet>();
        betSet.add(bet);
        betSet.add(bet2);
        //act
        BetResult betResult = gameRule.determineWinner(randomWinValue, betSet);
        //assert
        assertThat(betResult).isNotNull();
        assertThat(betResult.getAmountWon().getAmountInCents()).isEqualTo(2 * moneyAmount.getAmountInCents());
        assertThat(betResult.getWinningBet()).isIn(betSet);
    }
    @Test
    public void DetermineWinnerWhenNumberOfBetsIsLowerThan1ShouldReturnNull(){
        //arrange
        GameRule gameRule = new GameRule(maxBets);
        Set<Bet> betSet = new HashSet<Bet>();
        //act
        BetResult betResult = gameRule.determineWinner(randomWinValue, betSet);
        //assert
        assertThat(betResult).isNull();
    }
    @Test
    public void DetermineWinnerWhenNumberOfBetsIsBiggerThanMaxBetsShouldReturnNull(){
        //arrange
        GameRule gameRule = new GameRule(maxBets);
        MoneyAmount moneyAmount = mock(MoneyAmount.class);
        when(moneyAmount.getAmountInCents()).thenReturn((long)100);
        Bet bet = mock(Bet.class);
        when(bet.getMoneyAmount()).thenReturn(moneyAmount);
        Bet bet2 = mock(Bet.class);
        when(bet2.getMoneyAmount()).thenReturn(moneyAmount);
        Bet bet3 = mock(Bet.class);
        when(bet3.getMoneyAmount()).thenReturn(moneyAmount);
        Set<Bet> betSet = new HashSet<Bet>();
        betSet.add(bet);
        betSet.add(bet2);
        betSet.add(bet3);
        //act
        BetResult betResult = gameRule.determineWinner(randomWinValue, betSet);
        //assert
        assertThat(betResult).isNull();
    }
    @Test
    public void DetermineWinnerWhenRandomWinValueIsNullShouldThrowIllegalArgumentException(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        GameRule gameRule = new GameRule(maxBets);
        MoneyAmount moneyAmount = mock(MoneyAmount.class);
        when(moneyAmount.getAmountInCents()).thenReturn((long)100);
        Bet bet = mock(Bet.class);
        when(bet.getMoneyAmount()).thenReturn(moneyAmount);
        Set<Bet> betSet = new HashSet<Bet>();
        betSet.add(bet);
        gameRule.determineWinner(null, betSet);
    }
    @Test
    public void DetermineWinnerWhenBetsIsNullShouldThrowIllegalArgumentException(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        GameRule gameRule = new GameRule(maxBets);
        gameRule.determineWinner(randomWinValue, null);
    }
}