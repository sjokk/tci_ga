package casino.game;

import bettingauthoritiyAPI.BetLoggingAuthority;
import bettingauthoritiyAPI.BetToken;
import bettingauthoritiyAPI.BetTokenAuthority;
import bettingauthoritiyAPI.BettingAuthority;
import casino.bet.Bet;
import casino.bet.BetResult;
import casino.cashier.BetNotExceptedException;
import casino.cashier.PlayerCard;
import casino.gamingmachine.GamingMachine;
import casino.gamingmachine.IGamingMachine;
import casino.idfactory.*;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(JUnitParamsRunner.class)
public class GameTest {
    private IDFactory idFactoryMock;
    private GameRule gameRuleMock;
    private BettingAuthority bettingAuthorityMock;
    private BetTokenAuthority betTokenAuthorityMock;
    private BetLoggingAuthority betLoggingAuthorityMock;
    private Game game;
    private Bet betMock;
    private Bet betMock2;
    private IGamingMachine gamingMachineMock;
    private BettingRoundID bettingRoundIdMock;
    private GamingMachine gamingMachineMock2;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() {
        idFactoryMock = mock(IDFactory.class);
        gameRuleMock = mock(GameRule.class);
        bettingAuthorityMock = mock(BettingAuthority.class);
        betTokenAuthorityMock = mock(BetTokenAuthority.class);
        betLoggingAuthorityMock = mock(BetLoggingAuthority.class);
        betMock = mock(Bet.class);
        betMock2 = mock(Bet.class);
        gamingMachineMock = mock(GamingMachine.class);
        gamingMachineMock2 = mock(GamingMachine.class);
        bettingRoundIdMock = mock(BettingRoundID.class);

        when(bettingAuthorityMock.getTokenAuthority()).thenReturn(betTokenAuthorityMock);
        when(bettingAuthorityMock.getLoggingAuthority()).thenReturn(betLoggingAuthorityMock);

        game = new Game(idFactoryMock, gameRuleMock, bettingAuthorityMock);
    }

    @Test
    public void Game_checkIfCreatedObjectIsInstanceOfGame() {
        // Arrange
        game = new Game(idFactoryMock, gameRuleMock, bettingAuthorityMock);

        // Act

        // Assert
        assertThat("ERROR - game is not an instance of Game\n\n", game, instanceOf(Game.class));
        assertThat("ERROR - game.betTokenAuthority is not an instance of BetTokenAuthority\n\n",
                game.betTokenAuthority, instanceOf(BetTokenAuthority.class));
        assertThat("ERROR - game.betLoggingAuthority is not an instance of BetLoggingAuthority\n\n",
                game.betLoggingAuthority, instanceOf(BetLoggingAuthority.class));
    }

    private Object[] constructorArguments() {
        return new Object[]{
                new Object[]{null, null, null},
                new Object[]{new IDFactory(), null, null},
                new Object[]{new IDFactory(), new GameRule(5), null},
                new Object[]{new IDFactory(), null, new BettingAuthority()},
                new Object[]{null, new GameRule(5), null},
                new Object[]{null, new GameRule(5), new BettingAuthority()},
                new Object[]{null, null, new BettingAuthority()},
        };
    }

    @Test
    @Parameters(method = "constructorArguments")
    public void Game_idFactoryGameRuleAndBettingAuthorityShouldNotBeNull_IllegalArgumentException(
            IDFactory idFactoryMock, GameRule gameRuleMock, BettingAuthority bettingAuthority) {
        // Arrange
        exception.expect(IllegalArgumentException.class);
        game = new Game(idFactoryMock, gameRuleMock, bettingAuthority);

        // Act

        // Assert
    }

    @Test
    public void startBettingRound_whenNoRoundActiveStartOne() {
        // Arrange
        when(idFactoryMock.getID(ID.BETTING_ROUND)).thenReturn(bettingRoundIdMock);
        when(game.betTokenAuthority.getBetToken(bettingRoundIdMock)).thenReturn(mock(BetToken.class));

        // Act
        game.startBettingRound();

        // Assert
        verify(game.idFacory).getID(ID.BETTING_ROUND);
        verify(game.betTokenAuthority).getBetToken(bettingRoundIdMock);
        verify(game.betLoggingAuthority).startBettingRound(game.bettingRound);

        assertThat("ERROR - game.bettingRound is not an instance of BettingRound\n\n",
                game.bettingRound, instanceOf(BettingRound.class));
        assertThat("ERROR - Round is not active\n\n", game.roundActive, is(true));
    }

    @Test
    public void startBettingRound_whenRoundActiveAndCanNotStop_RoundNotFinishedException() {
        // Arrange
        exception.expect(RoundNotFinishedException.class);
        when(gameRuleMock.getMaxBetsPerRound()).thenReturn(2);

        // Act
        startValidBettingRound();
        startValidBettingRound();

        // Assert
    }

    @Test
    public void startBettingRound_whenRoundIsActiveAndCanStopDetermineWinnerAndStartNewRound() {
        // Act
        startGameWithTwoBetsAndTwoMaxBets();

        // Assert
        assertFalse("ERROR - List is empty\n\n", game.bettingRound.getAllBetsMade().isEmpty());

        // Act
        startValidBettingRound();

        // Assert
        verify(game.idFacory, times(2)).getID(ID.BETTING_ROUND);
        verify(game.betTokenAuthority, times(2)).getBetToken(bettingRoundIdMock);
        verify(game.betLoggingAuthority).startBettingRound(game.bettingRound);
        assertTrue("ERROR - List of bets is not empty\n\n", game.bettingRound.getAllBetsMade().isEmpty());
    }

    @Test
    public void acceptBet_roundMustBeActive_NoCurrentRoundException() {
        // Arrange
        exception.expect(NoCurrentRoundException.class);

        // Act
        game.acceptBet(betMock, gamingMachineMock);

        // Assert
    }

    private Object[] acceptBetParameters() {
        return new Object[]{
                new Object[]{null, null},
                new Object[]{betMock, null},
                new Object[]{null, gamingMachineMock},
        };
    }

    @Test
    @Parameters(method = "acceptBetParameters")
    public void acceptBet_gameMachineAndBetCanNotBeNull_IllegalArgumentException(Bet bet, IGamingMachine gamingMachine) {
        // Arrange
        exception.expect(IllegalArgumentException.class);

        // Act
        game.acceptBet(bet, gamingMachine);

        // Assert
    }

    @Test
    public void acceptBet_ifPlayerHasNotMadeBetPlaceOneAndReturnTrueAndIsLogged() throws BetNotExceptedException {
        // Arrange

        // Act
        startValidBettingRound();
        boolean betPlaced = game.acceptBet(betMock, gamingMachineMock);

        // Assert
        verify(betLoggingAuthorityMock).addAcceptedBet(betMock, bettingRoundIdMock, gamingMachineMock.getGamingMachineID());

        assertThat("ERROR - Bet did not get placed\n\n", betPlaced, is(true));
    }

    @Test
    public void acceptBet_ifPlayerHasAlreadyMadeABetReturnFalse() {
        // Arrange
        CardID cardIdMock = mock(CardID.class);
        Bet secondBetMock = mock(Bet.class);
        PlayerCard playerCardMock = mock(PlayerCard.class);

        when(betMock.getPlayerCard()).thenReturn(playerCardMock);
        when(secondBetMock.getPlayerCard()).thenReturn(playerCardMock);

        when(playerCardMock.getCardID()).thenReturn(cardIdMock);

        // Act
        startValidBettingRound();
        game.acceptBet(betMock, gamingMachineMock);
        boolean betPlaced = game.acceptBet(secondBetMock, gamingMachineMock);

        // Assert
        verify(betLoggingAuthorityMock, times(1))
                .addAcceptedBet(betMock, bettingRoundIdMock, gamingMachineMock.getGamingMachineID());

        assertThat("ERROR - Bet got placed\n\n", betPlaced, is(false));
    }

    @Test
    public void isBettingRoundFinished_returnFalseWhenNotAllNecessaryBetsAreMadeDeterminedByGameRules() {
        // Arrange
        boolean roundIsFinished;
        when(gameRuleMock.getMaxBetsPerRound()).thenReturn(5);

        // Act
        startValidBettingRound();
        roundIsFinished = game.isBettingRoundFinished();

        // Assert
        assertThat("ERROR - Round finished\n\n", roundIsFinished, is(false));
    }

    @Test
    public void isBettingRoundFinished_returnTrueWhenAllBetsMade() {
        // Arrange
        boolean roundIsFinished;

        // Act
        startGameWithTwoBetsAndTwoMaxBets();
        roundIsFinished = game.isBettingRoundFinished();

        // Assert
        assertThat("ERROR - Round did not finish\n\n", roundIsFinished, is(true));
    }

    @Test
    public void determineWinner_whenRoundIsFinishedDetermineWinnerAndSendResultToAllMachines() {
        // Arrange
        BetResult betResultMock = mock(BetResult.class);
        int randomWinValue = 1234;

        when(betTokenAuthorityMock.getRandomWinValue()).thenReturn(randomWinValue);

        // Act
        startGameWithTwoBetsAndTwoMaxBets();
        when(gameRuleMock.determineWinner(randomWinValue, game.bettingRound.getAllBetsMade())).thenReturn(betResultMock);
        game.determineWinner();

        // Assert
        verify(betTokenAuthorityMock).getRandomWinValue();
        verify(gameRuleMock).determineWinner(randomWinValue, game.bettingRound.getAllBetsMade());
        verify(gamingMachineMock).acceptWinner(betResultMock);
        verify(gamingMachineMock2).acceptWinner(betResultMock);
        verify(betLoggingAuthorityMock).endBettingRound(game.bettingRound, betResultMock);
    }

    @Test
    public void determineWinner_whenNoBetsMadeDoNotDoAnything() {
        // Arrange

        // Act
        startValidBettingRound();
        game.determineWinner();

        // Assert
        verify(betTokenAuthorityMock, never()).getRandomWinValue();
    }

    private void startValidBettingRound() {
        // Arrange
        when(idFactoryMock.getID(ID.BETTING_ROUND)).thenReturn(bettingRoundIdMock);
        when(game.betTokenAuthority.getBetToken(bettingRoundIdMock)).thenReturn(mock(BetToken.class));

        // Act
        game.startBettingRound();
    }

    private void startGameWithTwoBetsAndTwoMaxBets() {
        // Arrange
        PlayerCard playerCardMock1 = mock(PlayerCard.class);
        PlayerCard playerCardMock2 = mock(PlayerCard.class);

        CardID cardIdMock1 = mock(CardID.class);
        CardID cardIdMock2 = mock(CardID.class);

        when(betMock.getPlayerCard()).thenReturn(playerCardMock1);
        when(betMock2.getPlayerCard()).thenReturn(playerCardMock2);

        when(playerCardMock1.getCardID()).thenReturn(cardIdMock1);
        when(playerCardMock2.getCardID()).thenReturn(cardIdMock2);

        when(gameRuleMock.getMaxBetsPerRound()).thenReturn(2);

        // Act
        startValidBettingRound();
        game.acceptBet(betMock, gamingMachineMock);
        game.acceptBet(betMock2, gamingMachineMock2);
    }
}