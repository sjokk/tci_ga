package casino.game;

import bettingauthoritiyAPI.BetToken;
import casino.bet.Bet;
import casino.cashier.IPlayerCard;
import casino.idfactory.BettingRoundID;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class BettingRoundTest {

    private BetToken validBetToken = mock(BetToken.class);
    private BettingRoundID validBettingRoundID = mock(BettingRoundID.class);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void CreateBettingRound(){
        //arrange
        BettingRound bettingRound = new BettingRound(validBetToken, validBettingRoundID);
        //act
        //assert
        assertThat(bettingRound.getBetToken()).isEqualTo(validBetToken);
        assertThat(bettingRound.getBettingRoundID()).isEqualTo(validBettingRoundID);
    }
    @Test
    public void CreateBettingRoundWhereBetTokenIsNullShouldThrowIllegalArgumentException(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        BettingRound bettingRound = new BettingRound(null, validBettingRoundID);
    }
    @Test
    public void CreateBettingRoundWhereBettingRoundIDIsNullShouldThrowIllegalArgumentException(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        BettingRound bettingRound = new BettingRound(validBetToken, null);
    }
    @Test
    public void PlaceBetsFromDifferentGamblersShouldAddIt(){
        //arrange
        BettingRound bettingRound = new BettingRound(validBetToken, validBettingRoundID);
        Bet bet = mock(Bet.class);
        when(bet.getPlayerCard()).thenReturn(mock(IPlayerCard.class));
        Bet bet2 = mock(Bet.class);
        when(bet2.getPlayerCard()).thenReturn(mock(IPlayerCard.class));
        //act
        boolean result = bettingRound.placeBet(bet);
        boolean result2 = bettingRound.placeBet(bet2);
        //arrange
        assertThat(result).isTrue();
        assertThat(result2).isTrue();
        assertThat(bettingRound.getAllBetsMade()).hasSize(2).contains(bet).contains(bet2);
    }
    @Test
    public void PlaceBetsFromSameGamblersShouldNotAddIt(){
        //arrange
        BettingRound bettingRound = new BettingRound(validBetToken, validBettingRoundID);
        IPlayerCard card = mock(IPlayerCard.class);
        Bet bet = mock(Bet.class);
        when(bet.getPlayerCard()).thenReturn(card);
        Bet bet2 = mock(Bet.class);
        when(bet2.getPlayerCard()).thenReturn(card);
        //act
        boolean result = bettingRound.placeBet(bet);
        boolean result2 = bettingRound.placeBet(bet2);
        //arrange
        assertThat(result).isTrue();
        assertThat(result2).isFalse();
        assertThat(bettingRound.getAllBetsMade()).hasSize(1).contains(bet);
    }
    @Test
    public void PlaceBetWhichIsNullShouldThrowIllegalArgumentException(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        BettingRound bettingRound = new BettingRound(validBetToken, validBettingRoundID);
        //act
        bettingRound.placeBet(null);
    }
    @Test
    public void GetNumberOfBetsMadeShouldReturnTheNumberOfBetsPlaced(){
        //arrange
        BettingRound bettingRound = new BettingRound(validBetToken, validBettingRoundID);
        Bet bet = mock(Bet.class);
        when(bet.getPlayerCard()).thenReturn(mock(IPlayerCard.class));
        Bet bet2 = mock(Bet.class);
        when(bet2.getPlayerCard()).thenReturn(mock(IPlayerCard.class));
        //act
        boolean result = bettingRound.placeBet(bet);
        boolean result2 = bettingRound.placeBet(bet2);
        //assert
        assertThat(bettingRound.numberOFBetsMade()).isEqualTo(2);
    }
    @Test
    public void GetNumberOfBetsMadeWhenNoBetsPlacedShouldReturn0(){
        //arrange
        BettingRound bettingRound = new BettingRound(validBetToken, validBettingRoundID);
        //act
        //assert
        assertThat(bettingRound.numberOFBetsMade()).isEqualTo(0);
    }
}