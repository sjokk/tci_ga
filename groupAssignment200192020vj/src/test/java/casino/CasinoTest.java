package casino;

import casino.bet.Bet;
import casino.cashier.BetNotExceptedException;
import casino.cashier.ICashier;
import casino.cashier.IPlayerCard;
import casino.game.IGame;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class CasinoTest {
        private static final ICasino CASINO = new Casino();
        private static final IGame GAME_TO_ADD = mock(IGame.class);

        private static final String EMPTY_STRING = "";
        private static final String BLACKJACK = "Blackjack";

        @Rule public ExpectedException thrown = ExpectedException.none();

        /**
         * Tests direct input: gameName
         *
         * Behaviour tested: Exception thrown on illegal input.
         */
        @Test
        public void addGame_gameNameEmptyString_IllegalArgumentExceptionShouldBeThrown() throws DuplicateGameException {
                thrown.expect(IllegalArgumentException.class);
                // Arrange
                // Act
                CASINO.addGame(EMPTY_STRING, GAME_TO_ADD);

                // Assert
        }

        /**
         * Tests direct input: gameName
         *
         * Behaviour tested: Exception thrown on illegal input.
         */
        @Test
        public void addGame_gameNameNull_IllegalArgumentExceptionShouldBeThrown() throws DuplicateGameException {
                thrown.expect(IllegalArgumentException.class);
                // Arrange
                // Act
                CASINO.addGame(null, GAME_TO_ADD);

                // Assert
        }

        /**
         * Tests direct input: gameToAdd
         *
         * Behaviour tested: Exception thrown on illegal input.
         */
        @Test
        public void addGame_nullGivenForGameToAdd_IllegalArgumentExceptionShouldBeThrown() throws DuplicateGameException {
                thrown.expect(IllegalArgumentException.class);
                // Arrange
                // Act
                CASINO.addGame(BLACKJACK, null);

                // Assert
        }

        /**
         * Tests direct inputs: gameName, gameToAdd
         *
         * Behaviour tested: Exception thrown on duplicate game.
         */
        @Test
        public void addGame_duplicateGameNameAndGameTypeGiven_DuplicateGameExceptionShouldBeThrown() throws DuplicateGameException {
                thrown.expect(DuplicateGameException.class);
                // Arrange
                // Act
                CASINO.addGame(BLACKJACK, GAME_TO_ADD);
                CASINO.addGame(BLACKJACK, GAME_TO_ADD);
                // Assert
        }

        /**
         * Tests direct inputs: gameName, gameToAdd
         *
         * Behaviour tested: Adding a game to the casino.
         */
        @Test
        public void addGame_uniqueGameNameAndGameTypeGiven_GameSuccessfullyAddedToCasino() throws DuplicateGameException {
                // Arrange
                CASINO.addGame(BLACKJACK, GAME_TO_ADD);

                // Act
                IGame addedGame = CASINO.getGame(BLACKJACK);

                // Assert
                assertEquals("The added game did not match the game returned by getGame!", GAME_TO_ADD, addedGame);
        }

        /**
         * Tests direct inputs: card and betToCheck
         *
         * Behaviour tested: Exception thrown on illegal input.
         */
        @Test
        public void checkIfBetIsValid_nullInputForCard_IllegalArgumentExceptionShouldBeThrown() throws BetNotExceptedException {
                thrown.expect(IllegalArgumentException.class);
                // Arrange
                Bet betToCheck = mock(Bet.class);

                // Act
                CASINO.checkIfBetIsValid(null, betToCheck);

                // Assert
        }

        /**
         * Tests direct inputs: card and betToCheck
         *
         * Behaviour tested: Exception thrown on illegal input.
         */
        @Test
        public void checkIfBetIsValid_nullInputForBet_IllegalArgumentExceptionShouldBeThrown() throws BetNotExceptedException {
                thrown.expect(IllegalArgumentException.class);
                // Arrange
                IPlayerCard card = mock(IPlayerCard.class);

                // Act
                CASINO.checkIfBetIsValid(card, null);

                // Assert
        }

        /**
         * Tests direct inputs: card and betToCheck
         * Indirect outputs: card and betToCheck for Cashier
         * Indirect inputs: result of Cashier's checkIfBetIsValid
         * Direct output: the result of Cashier's checkIfBetIsValid returned by the Casino
         *
         * Behaviour tested: A valid bet is made and player balance is sufficient.
         */
        @Test
        public void checkIfBetIsValid_cardWithBalanceEqualToAmountBet_trueReturned() throws BetNotExceptedException {
                // Arrange
                IPlayerCard card = mock(IPlayerCard.class);
                Bet betToCheck = mock(Bet.class);
                ICashier cashier = mock(ICashier.class);
                ICasino casino = new Casino(cashier);

                when(cashier.checkIfBetIsValid(card, betToCheck)).thenReturn(true);

                // Act
                boolean isBetValid = casino.checkIfBetIsValid(card, betToCheck);

                // Assert
                verify(cashier).checkIfBetIsValid(card, betToCheck);
        }
}
