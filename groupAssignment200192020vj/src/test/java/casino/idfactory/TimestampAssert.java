package casino.idfactory;

import org.fest.assertions.Assertions;
import org.fest.assertions.GenericAssert;
import org.fest.assertions.LongAssert;

import java.sql.Timestamp;

public class TimestampAssert extends GenericAssert<TimestampAssert, Timestamp> {
    public TimestampAssert(Timestamp actual) {
        super(TimestampAssert.class, actual);
    }

    public static TimestampAssert assertThat(Timestamp actual) {
        return new TimestampAssert(actual);
    }

    public TimestampAssert isBetween(Timestamp start, Timestamp end) {
        this.isNotNull();

        String errorMessage = String.format("Expected timestamp to be between <%s> and <%s> but was <%s>", start.getTime(), end.getTime(), ((Timestamp)this.actual).getTime());

        Assertions.assertThat(actual.getTime())
                .overridingErrorMessage(errorMessage)
                .isGreaterThanOrEqualTo(start.getTime());

        Assertions.assertThat(actual.getTime())
                .overridingErrorMessage(errorMessage)
                .isLessThanOrEqualTo(end.getTime());

        return this;
    }
}
