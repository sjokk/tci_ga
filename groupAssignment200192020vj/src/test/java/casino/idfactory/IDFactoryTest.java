package casino.idfactory;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.fest.assertions.Assertions.*;

@RunWith(JUnitParamsRunner.class)
public class IDFactoryTest {
    private static final IDFactory FACTORY = new IDFactory();

    /**
     * Tests direct inputs (enum ID) and outputs (GeneralID object)
     *
     * Method tests the correct construction and returning a GeneralID object based on
     * an enum ID input.
     */
    @Test
    @Parameters(method = "getExistingIDTypes")
    public void getID_existingIDEnumerationAsInputParameter_correspondingGeneralIDObjectReturned(ID existingType, GeneralID expectedID) {
        // Act
        GeneralID actualID = FACTORY.getID(existingType);

        // Assert
        assertThat(actualID).isInstanceOf(expectedID.getClass());
    }

    private static final Object[] getExistingIDTypes() {
        return new Object[] {
            new Object[] { ID.CARD, new CardID() },
            new Object[] { ID.BETTING_ROUND, new BettingRoundID() },
            new Object[] { ID.BET, new BetID() },
            new Object[] { ID.GAMING_MACHINE, new GamingMachineID() }
        };
    }

    /**
     * Tests direct inputs (null value) and output null
     *
     * Method tests the correct behaviour when a null value is given
     * to the method.
     */
    @Test
    public void getID_nullAsParameter_nullReturned() {
        // Arrange
        ID nonexistentId = null;

        // Act
        GeneralID actualID = FACTORY.getID(nonexistentId);

        // Assert
        assertThat(actualID).isNull();
    }
}