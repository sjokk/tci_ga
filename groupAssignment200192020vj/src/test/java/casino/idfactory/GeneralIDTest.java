package casino.idfactory;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.Timestamp;
import java.util.UUID;

import static org.fest.assertions.Assertions.*;

@RunWith(JUnitParamsRunner.class)
public class GeneralIDTest {

    private static final int BEFORE = -1;
    private static final int EQUAL = 0;
    private static final int AFTER = 1;

    /**
     * Tests direct outputs of the getUniqueID and getTimestamp methods.
     *
     * Tests if the ID has values assigned after construction, verifying proper construction.
     */
    @Test
    public void constructor_onConstructionOfID_timestampAndUniqueIDHaveValuesAssigned() {
        // Arrange
        GeneralID existingId = new BetID();

        // Act
        UUID validID = existingId.getUniqueID();
        Timestamp validTimestamp = existingId.getTimestamp();

        // Assert
        assertThat(validID).isNotNull();
        assertThat(validTimestamp).isNotNull();
    }

    /**
     * Tests direct output of the getTimestamp methods.
     *
     * Tests if the timestamp of creating the ID is start_timestamp <= construction_timestamp <= end_timestamp
     * testing if the object has a correct timestamp assigned after construction.
     *
     * (Shamefully this more so tests timestamps behaviour than our class, although I made a custom matcher for it).
     */
    @Test
    public void constructor_onConstructionOfID_idTimestampBetweenMethodStartTimestampAndMethodEndTimestamp() {
        // Arrange
        Timestamp start = new Timestamp(System.currentTimeMillis());
        GeneralID existingId = new BetID();
        Timestamp end = new Timestamp(System.currentTimeMillis());

        // Act
        Timestamp actualTimestamp = existingId.getTimestamp();

        // Assert
        TimestampAssert.assertThat(actualTimestamp).isBetween(start, end);
    }

    /**
     * Tests direct output of the compareTo method.
     *
     * Tests comparing two ids, created one after the other, testing if
     * the second comes after the first and first comes before the second.
     */
    @Test
    public void compareTo_onConstructionOfTwoIDs_firstIDCreatedShouldComeBeforeSecondID() {
        // Arrange
        GeneralID firstID = new BetID();
        GeneralID secondID = new CardID();

        // Act
        int firstComparedToSecond = firstID.compareTo(secondID);
        int secondComparedToFirst = secondID.compareTo(firstID);

        // (Because this is based on random generation of UUID I have to add an conditional check for now).
        // Swap before and after.

        // Assert
        if (firstComparedToSecond < 0) {
            Assertions.assertThat(firstComparedToSecond).isEqualTo(BEFORE);
            Assertions.assertThat(secondComparedToFirst).isEqualTo(AFTER);
        } else {
            Assertions.assertThat(firstComparedToSecond).isEqualTo(AFTER);
            Assertions.assertThat(secondComparedToFirst).isEqualTo(BEFORE);
        }
    }

    /**
     * Tests direct output: result of compareTo
     *
     * Behaviour tested: An ID created at a later time than an ID created before
     * should be considered AFTER, and equivalently the ID created before, should
     * be considered BEFORE.
     */
    @Test
    public void compareTo_onConstructionOfTwoIDsOneMilliSecondInBetween_firstIdShouldBeBeforeSecondId() throws InterruptedException {
        // Arrange
        GeneralID firstId = new BetID();
        Thread.sleep(1);
        GeneralID secondId = new CardID();

        // Act
        int firstIdBeforeSecond = firstId.compareTo(secondId);
        int secondIfAfterFirst = secondId.compareTo(firstId);

        // Assert
        assertThat(firstIdBeforeSecond).isEqualTo(BEFORE);
        assertThat(secondIfAfterFirst).isEqualTo(AFTER);
    }

    /**
     * Tests direct outputs: result of compareTo method.
     *
     * Behaviour tested: object equivalence.
     */
    @Test
    public void compareTo_twoIdenticalIDs_objectsShouldBeConsideredEqual() {
        // Arrange
        GeneralID id = new GamingMachineID();
        GeneralID identicalId = id;

        // Act
        int equalIds = id.compareTo(identicalId);

        // Assert
        assertThat(equalIds).isEqualTo(EQUAL);
    }

    /**
     * Tests direct inputs: null and a general Object. Direct output: 'IllegalArgumentException'.
     */
    @Test(expected = IllegalArgumentException.class)
    @Parameters(method = "getNonComparableObjects")
    public void compareTo_invalidObjectsForComparison_assertionErrorShouldBeThrown(Object nonComparableObject) {
        // Arrange
        GeneralID id = new GamingMachineID();

        // Act // Assert
        id.compareTo(nonComparableObject);
    }

    public Object[] getNonComparableObjects() {
        return new Object[] {
            new Object[] { null },
            new Object[] { new Object() }
        };
    }

    /**
     * Behaviour to be tested: Test if two different ID objects are not equal
     */
    @Test
    public void equals_twoDifferentIDObjects_expectedFalseReturned() {
        // Arrange
        GeneralID idOne = new BetID();
        GeneralID idTwo = new BetID();

        // Act
        boolean areEqual = idOne.equals(idTwo);

        // Assert
        Assert.assertFalse("Compared objects are equal, should be unequal!", areEqual);
    }

    /**
     * Behaviour to be tested: Test if two different ID types are not equal
     */
    @Test
    public void equals_twoDifferentTypeOfIDObjects_expectedFalseReturned() {
        // Arrange
        GeneralID betId = new BetID();
        GeneralID cardId = new CardID();

        // Act
        boolean areEqual = betId.equals(cardId);

        // Assert
        Assert.assertFalse("Different types of IDs are considered equal, should be unequal!", areEqual);
    }

    /**
     * Behaviour to be tested: Test if two equivalent ID objects are equal
     */
    @Test
    public void equals_twoEquivalentIDObjects_expectedTrueReturned() {
        // Arrange
        GeneralID id = new BettingRoundID();
        GeneralID identicalId = id;

        // Act
        boolean areEqual = id.equals(identicalId);

        // Assert
        Assert.assertTrue("Equivalent objects were not considered equal!", areEqual);
    }

    /**
     * Behaviour to be tested: Test if two different types of objects are not equal
     */
    @Test
    @Parameters(method = "getDifferingTypesOfObjects")
    public void equals_twoDifferentTypeOfObjects_expectedFalseReturned(Object nonIDObject) {
        // Arrange
        GeneralID id = new BettingRoundID();

        // Act
        boolean areEqual = id.equals(nonIDObject);

        // Assert
        Assert.assertFalse("Different type of objects were considered equal!", areEqual);
    }

    public Object[] getDifferingTypesOfObjects() {
        return new Object[] {
                new Object[] { null },
                new Object[] { new Object() }
         };
    }
}