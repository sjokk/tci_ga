package casino.gamingmachine;

import casino.bet.Bet;
import casino.bet.BetResult;
import casino.cashier.BetNotExceptedException;
import casino.cashier.ICashier;
import casino.cashier.IPlayerCard;
import casino.game.IGame;
import casino.game.NoCurrentRoundException;
import casino.idfactory.GamingMachineID;
import casino.idfactory.ID;
import casino.idfactory.IDFactory;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class GamingMachineTest implements IObserver {
    private ICashier cashier;
    private Bet lastAttemptedBet;

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private MethodChoice choice;
    private IPlayerCard playerCard;
    private IGame game;
    private GamingMachine gamingMachine;

    /**
     * Tests direct input: IPlayerCard
     * <p>
     * Behaviour tested: Exception thrown when illegal argument input.
     */
    @Test
    public void connectCard_inputPlayerCardIsNull_illegalArgumentExceptionShouldBeThrown() {
        thrown.expect(IllegalArgumentException.class);

        // Arrange
        IGamingMachine gamingMachine = new GamingMachine();

        // Act
        gamingMachine.connectCard(null);

        // Assert
    }

    /**
     * Tests direct input: IPlayerCard
     * direct output: IPlayerCard
     * <p>
     * Behaviour tested: Correct assignment of PlayerCard to machine.
     */
    @Test
    public void connectCard_existingPlayerCardIsInput_cardShouldBeConnectedToMachine() {
        // Arrange
        IGamingMachine gamingMachine = new GamingMachine();
        IPlayerCard playerCard = mock(IPlayerCard.class);

        // Act
        gamingMachine.connectCard(playerCard);
        IPlayerCard connectedCard = gamingMachine.getPlayerCard();

        // Assert
        assertSame("Player card connected is not the same!!", playerCard, connectedCard);
    }

    /**
     * Tests direct input: GamingMachineID
     * direct output: GamingMachineID
     * <p>
     * Behaviour tested: Constructed with a non null gaming machine id.
     */
    @Test
    public void getGamingMachineID_constructGamingMachine_nonNullIdReturned() {
        // Arrange
        IDFactory idFactory = new IDFactory();

        GamingMachineID actualId = (GamingMachineID) idFactory.getID(ID.GAMING_MACHINE);
        IGamingMachine gamingMachine = new GamingMachine(actualId);

        // Act
        GamingMachineID gamingMachineID = gamingMachine.getGamingMachineID();

        // Assert
        assertNotNull("Construction failed, gaming machine id was null!", gamingMachineID);
    }

    /**
     * Tests direct inputs: BetResult
     * indirect output: PlayerCard and BetResult (Passed to cashier)
     * <p>
     * Even though there are no indirect inputs (only void returns)
     * <p>
     * Behaviour tested: GamingMachine correctly clears bets from machine and cashier does not
     * call addAmount when player on this gaming machine did not win.
     */
    @Test
    public void acceptWinner_betResultInputWinningBetNotBelongingToPlayerOnThisMachine_cashierShouldNotHaveAddedAmount() throws NoPlayerCardException {
        // Arrange
        playerCard = mock(IPlayerCard.class);
        game = mock(IGame.class);
        cashier = mock(ICashier.class);
        BetResult winResult = mock(BetResult.class);
        long betAmount = 100;
        Bet randomBetNotBelongingToPlayer = mock(Bet.class);
        gamingMachine = new GamingMachine(cashier, game);

        // Act
        gamingMachine.connectCard(playerCard);
        gamingMachine.attach(this);

        gamingMachine.placeBet(betAmount);
        when(winResult.getWinningBet()).thenReturn(randomBetNotBelongingToPlayer);
        gamingMachine.acceptWinner(winResult);

        // Assert
        verify(cashier, never()).addAmount(playerCard, winResult.getAmountWon());
        assertNull("Bets should be cleared after winner is announced!", gamingMachine.getMadeBet());
    }

    /**
     * Tests direct inputs: BetResult
     * indirect output: PlayerCard and BetResult (Passed to cashier)
     * <p>
     * Even though there are no indirect inputs (only void returns)
     * <p>
     * Behaviour tested: GamingMachine correctly clears bets from machine and cashier does not
     * call addAmount when player on this gaming machine DID win.
     */
    @Test
    public void acceptWinner_betResultInputWinningBetDoesBelongToPlayerOnThisMachine_cashierShouldHaveAddedAmount() throws NoPlayerCardException {
        // Arrange
        playerCard = mock(IPlayerCard.class);
        game = mock(IGame.class);
        cashier = mock(ICashier.class);
        BetResult winResult = mock(BetResult.class);
        long betAmount = 100;
        gamingMachine = new GamingMachine(cashier, game);

        // Act
        gamingMachine.connectCard(playerCard);
        gamingMachine.attach(this);

        gamingMachine.placeBet(betAmount);
        when(winResult.getWinningBet()).thenReturn(gamingMachine.getMadeBet());
        gamingMachine.acceptWinner(winResult);

        // Assert
        verify(cashier).addAmount(playerCard, winResult.getAmountWon());
        assertNull("Bets should be cleared after winner is announced!", gamingMachine.getMadeBet());
    }

    /**
     * Direct inputs: Long amountInCents (bet amount).
     * <p>
     * Behaviour: Exception thrown when there is no player card connected.
     */
    @Test
    public void placeBet_placeBetWhenNoPlayerCardConnected_noPlayerCardExceptionShouldBeThrown() throws NoPlayerCardException {
        thrown.expect(NoPlayerCardException.class);

        // Arrange
        IGamingMachine gamingMachine = new GamingMachine();
        long betAmount = 100;

        // Act
        gamingMachine.placeBet(betAmount);

        // Assert
    }

    /**
     * Direct inputs: Long amountInCents (bet amount).
     * Indirect outputs: IPlayerCard and Bet for (ICashier).
     * Indirect outputs: Bet and IGamingMachine for (IGame).
     * Indirect inputs: result from ICashier's checkIfBetIsValid method.
     * Indirect inputs: result from IGame's acceptBet method.
     * Direct outputs: placeBet method result.
     * <p>
     * Behaviour: An attempt to place a bet is made, while player
     * does not have enough credit.
     */
    @Test
    public void placeBet_placeBetWhenPlayerCardAmountIsTooLittle_falseShouldBeReturned() throws NoPlayerCardException, BetNotExceptedException {
        // Arrange
        playerCard = mock(IPlayerCard.class);
        cashier = mock(ICashier.class);
        long betAmount = 100;
        GamingMachine gamingMachine = new GamingMachine(cashier);
        choice = MethodChoice.CASHIER_INVALIDATE;

        // Act
        gamingMachine.connectCard(playerCard);
        gamingMachine.attach(this);

        boolean betPlaced = gamingMachine.placeBet(betAmount);
        when(cashier.checkIfBetIsValid(playerCard, lastAttemptedBet)).thenReturn(false);

        // Assert
        verify(cashier).checkIfBetIsValid(playerCard, lastAttemptedBet);
        assertFalse("The placed bet should result in false (not placed)!", betPlaced);
    }

    /**
     * Direct inputs: Long amountInCents (bet amount).
     * Indirect outputs: IPlayerCard and Bet for (ICashier).
     * Indirect outputs: Bet and IGamingMachine for (IGame).
     * Indirect inputs: result from ICashier's checkIfBetIsValid method.
     * Indirect inputs: result from IGame's acceptBet method.
     * Direct outputs: placeBet method result.
     * <p>
     * Behaviour: An attempt to place a bet is made, while the player
     * already has made a bet on that game's round before.
     */
    @Test
    public void placeBet_placeBetOnGameWhenPlayerAlreadyMadeABet_falseShouldBeReturned() throws NoPlayerCardException, NoCurrentRoundException, BetNotExceptedException {
        // Arrange
        playerCard = mock(IPlayerCard.class);
        IGame game = mock(IGame.class);
        choice = MethodChoice.GAME_INVALIDATE;
        cashier = mock(ICashier.class);
        long betAmount = 100;
        GamingMachine gamingMachine = new GamingMachine(cashier, game);

        // Act
        gamingMachine.connectCard(playerCard);
        gamingMachine.attach(this);

        boolean betPlaced = gamingMachine.placeBet(betAmount);
        when(game.acceptBet(lastAttemptedBet, gamingMachine)).thenReturn(false);

        // Assert
        verify(game).acceptBet(lastAttemptedBet, gamingMachine);
        assertFalse("The placed bet should result in false (not placed)!", betPlaced);
    }

    /**
     * Direct inputs: Long amountInCents (bet amount).
     * Indirect outputs: IPlayerCard and Bet for (ICashier).
     * Indirect outputs: Bet and IGamingMachine for (IGame).
     * Indirect inputs: result from ICashier's checkIfBetIsValid method.
     * Indirect inputs: result from IGame's acceptBet method.
     * Direct outputs: placeBet method result.
     * <p>
     * Behaviour: An attempt to place a bet is made, player has enough
     * credit on card and did not yet bet on current game's betting round.
     */
    @Test
    public void placeBet_placeBetWhenPlayerCardHasEnoughCreditAndAllowedToBetOnGame_trueShouldBeReturned() throws NoPlayerCardException {
        // Arrange
        playerCard = mock(IPlayerCard.class);
        game = mock(IGame.class);
        cashier = mock(ICashier.class);
        choice = MethodChoice.VALID;
        long betAmount = 100;
        gamingMachine = new GamingMachine(cashier, game);

        // Act
        gamingMachine.connectCard(playerCard);
        gamingMachine.attach(this);

        boolean betPlaced = gamingMachine.placeBet(betAmount);

        // Assert
        assertTrue("Bet should be made, system tells it was not!", betPlaced);
        assertNotNull("The made bet should not be null if the bet succeeded!", gamingMachine.getMadeBet());
    }

    /**
     * Method is used to 'loosely couple' the test class and the actual
     * class since one method placeBet has a hard dependency (Bet class)
     * which otherwise could not be verified.
     *
     * This method acts as part of the observer, to set the appropriate
     * mocking return values based on which test method is executed, to ensure
     * that the right values are returned and behaviour can be verified.
     *
     * @param lastAttemptedBet
     */
    @Override
    public void update(Bet lastAttemptedBet) {
        if (choice == null) {
            choice = MethodChoice.VALID;
        }

        this.lastAttemptedBet = lastAttemptedBet;

        try {
            switch (choice) {
                case CASHIER_INVALIDATE:
                    when(cashier.checkIfBetIsValid(playerCard, lastAttemptedBet)).thenReturn(false);
                    break;
                case GAME_INVALIDATE:
                    when(cashier.checkIfBetIsValid(playerCard, lastAttemptedBet)).thenReturn(true);
                    break;
                default:
                    when(cashier.checkIfBetIsValid(playerCard, lastAttemptedBet)).thenReturn(true);
                    when(game.acceptBet(lastAttemptedBet, gamingMachine)).thenReturn(true);
                    break;
            }
        } catch (BetNotExceptedException | NoCurrentRoundException e) {
            e.printStackTrace();
        }
    }
}
