package casino.cashier;

import bettingauthoritiyAPI.BetLoggingAuthority;
import casino.bet.Bet;
import casino.bet.MoneyAmount;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class CashierTest {

    private IPlayerCard validPlayerCard = mock(IPlayerCard.class);
    private BetLoggingAuthority validBetLoggingAuthority = mock(BetLoggingAuthority.class);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void CreateCashier(){
        //arrange
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        validPlayerCards.add(validPlayerCard);

        Cashier cashier = new Cashier(validPlayerCards, validBetLoggingAuthority);
        //act
        //assert
        assertThat(cashier.getPlayerCards()).isEqualTo(validPlayerCards);
        assertThat(cashier.getBetLoggingAuthority()).isEqualTo(validBetLoggingAuthority);
    }
    @Test
    public void CreateCashierWherePlayerCardsIsNullShouldThrowIllegalArgumentException(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        Cashier cashier = new Cashier(null, validBetLoggingAuthority);
    }
    @Test
    public void CreateCashierWhereBetLoggingAuthorityIsNullShouldThrowIllegalArgumentException(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        validPlayerCards.add(validPlayerCard);
        Cashier cashier = new Cashier(validPlayerCards, null);
    }
    @Test
    public void DistributeCardShouldReturnACardWhenNumberOfCardsIsGreaterThan0(){
        //arrange
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        validPlayerCards.add(validPlayerCard);
        Cashier cashier = new Cashier(validPlayerCards, validBetLoggingAuthority);
        //act
        IPlayerCard playerCard = cashier.distributeGamblerCard();
        //assert
        assertThat(playerCard).isEqualTo(validPlayerCard);
        assertThat(cashier.getDistributedPlayerCards()).hasSize(1).contains(validPlayerCard);
        assertThat(cashier.getDistributedPlayerCardsAmount()).hasSize(1);
        assertThat(cashier.getDistributedPlayerCardsAmount().get(0).getAmountInCents()).isEqualTo((long)0);
        assertThat(cashier.getPlayerCards()).hasSize(0);
        verify(validBetLoggingAuthority).handOutGamblingCard(playerCard.getCardID());
    }
    @Test
    public void DistributeCardShouldReturnNullWhenNumberOfCardsIs0(){
        //arrange
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        Cashier cashier = new Cashier(validPlayerCards, validBetLoggingAuthority);
        //act
        IPlayerCard playerCard = cashier.distributeGamblerCard();
        //assert
        assertThat(playerCard).isNull();
        assertThat(cashier.getDistributedPlayerCards()).hasSize(0);
        assertThat(cashier.getDistributedPlayerCardsAmount()).hasSize(0);
        assertThat(cashier.getPlayerCards()).hasSize(0);
    }
    @Test
    public void ReturnCard(){
        //arrange
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        validPlayerCards.add(validPlayerCard);
        Cashier cashier = new Cashier(validPlayerCards, validBetLoggingAuthority);
        //act
        IPlayerCard receivedCard = cashier.distributeGamblerCard();
        cashier.returnGamblerCard(receivedCard);
        //assert
        assertThat(cashier.getPlayerCards()).hasSize(1).contains(receivedCard);
        assertThat(cashier.getDistributedPlayerCards()).hasSize(0);
        assertThat(cashier.getDistributedPlayerCardsAmount()).hasSize(0);
        verify(receivedCard).returnBetIDsAndClearCard();
        verify(validBetLoggingAuthority).handInGamblingCard(receivedCard.getCardID(), receivedCard.returnBetIDs());
    }
    @Test
    public void WhenCardIsReturnedWhichIsNotDistributedShouldThrowIllegalArgumentException(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        IPlayerCard notDistributedCard = mock(IPlayerCard.class);
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        validPlayerCards.add(validPlayerCard);
        Cashier cashier = new Cashier(validPlayerCards, validBetLoggingAuthority);
        //act
        cashier.returnGamblerCard(notDistributedCard);
    }
    @Test
    public void WhenCardIsReturnedWhichIsNullShouldThrowIllegalArgumentException(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        IPlayerCard nullCard = null;
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        validPlayerCards.add(validPlayerCard);
        Cashier cashier = new Cashier(validPlayerCards, validBetLoggingAuthority);
        //act
        cashier.returnGamblerCard(nullCard);
    }
    @Test
    public void AddAmountToPlayerCard(){
        //arrange
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        validPlayerCards.add(validPlayerCard);
        Cashier cashier = new Cashier(validPlayerCards, validBetLoggingAuthority);

        MoneyAmount moneyAmount = mock(MoneyAmount.class);
        when(moneyAmount.getAmountInCents()).thenReturn((long)100);
        //act
        IPlayerCard receivedCard = cashier.distributeGamblerCard();
        cashier.addAmount(receivedCard, moneyAmount);
        //assert
        assertThat(cashier.getDistributedPlayerCardsAmount().get(0).getAmountInCents()).isEqualTo(moneyAmount.getAmountInCents());
    }
    @Test
    public void AddAmountToPlayerCardShouldThrowIllegalArgumentExceptionWhenCardIsNull(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        IPlayerCard nullCard = null;
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        validPlayerCards.add(validPlayerCard);
        Cashier cashier = new Cashier(validPlayerCards, validBetLoggingAuthority);

        MoneyAmount moneyAmount = mock(MoneyAmount.class);
        when(moneyAmount.getAmountInCents()).thenReturn((long)100);
        //act
        cashier.addAmount(nullCard, moneyAmount);
    }
    @Test
    public void AddAmountToPlayerCardShouldThrowIllegalArgumentExceptionWhenAmountIsNull(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        validPlayerCards.add(validPlayerCard);
        Cashier cashier = new Cashier(validPlayerCards, validBetLoggingAuthority);

        MoneyAmount moneyAmount = null;
        //act
        IPlayerCard card = cashier.distributeGamblerCard();
        cashier.addAmount(card, moneyAmount);
    }
    @Test
    public void AddAmountToPlayerCardShouldThrowIllegalArgumentExceptionWhenCardIsNotDistributed(){
        //arrange
        thrown.expect(IllegalArgumentException.class);
        IPlayerCard notDistributedCard = mock(IPlayerCard.class);
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        validPlayerCards.add(validPlayerCard);
        Cashier cashier = new Cashier(validPlayerCards, validBetLoggingAuthority);

        MoneyAmount moneyAmount = mock(MoneyAmount.class);
        when(moneyAmount.getAmountInCents()).thenReturn((long)100);
        //act
        cashier.addAmount(notDistributedCard, moneyAmount);
    }
    @Test
    public void CheckBetIsValidWhenEnoughMoneyOnCardComparedToBetShouldReturnTrueAndDecreaseMoneyOnCard() throws BetNotExceptedException {
        //arrange
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        validPlayerCards.add(validPlayerCard);
        Cashier cashier = new Cashier(validPlayerCards, validBetLoggingAuthority);

        MoneyAmount moneyAmount = mock(MoneyAmount.class);
        when(moneyAmount.getAmountInCents()).thenReturn((long)100);
        MoneyAmount moneyAmount2 = mock(MoneyAmount.class);
        when(moneyAmount2.getAmountInCents()).thenReturn((long)20);
        Bet bet = mock(Bet.class);
        when(bet.getMoneyAmount()).thenReturn(moneyAmount2);
        //act
        IPlayerCard receivedCard = cashier.distributeGamblerCard();
        cashier.addAmount(receivedCard, moneyAmount);
        boolean isValid = cashier.checkIfBetIsValid(receivedCard, bet);
        //assert
        assertThat(isValid).isEqualTo(true);
        assertThat(cashier.getDistributedPlayerCardsAmount().get(0).getAmountInCents()).isEqualTo((long)80);
    }
    @Test
    public void CheckBetIsValidWhenNotEnoughMoneyOnCardComparedToBetShouldReturnFalse() throws BetNotExceptedException {
        //arrange
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        validPlayerCards.add(validPlayerCard);
        Cashier cashier = new Cashier(validPlayerCards, validBetLoggingAuthority);

        MoneyAmount moneyAmount = mock(MoneyAmount.class);
        when(moneyAmount.getAmountInCents()).thenReturn((long)100);
        MoneyAmount moneyAmount2 = mock(MoneyAmount.class);
        when(moneyAmount2.getAmountInCents()).thenReturn((long)120);
        Bet bet = mock(Bet.class);
        when(bet.getMoneyAmount()).thenReturn(moneyAmount2);
        //act
        IPlayerCard receivedCard = cashier.distributeGamblerCard();
        cashier.addAmount(receivedCard, moneyAmount);
        boolean isValid = cashier.checkIfBetIsValid(receivedCard, bet);
        //assert
        assertThat(isValid).isEqualTo(false);
        assertThat(cashier.getDistributedPlayerCardsAmount().get(0).getAmountInCents()).isEqualTo((long)100);
    }
    @Test
    public void CheckBetIsValidWhenCardIsNullShouldThrowIllegalArgumentException() throws BetNotExceptedException {
        //arrange
        thrown.expect(IllegalArgumentException.class);
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        validPlayerCards.add(validPlayerCard);
        Cashier cashier = new Cashier(validPlayerCards, validBetLoggingAuthority);

        MoneyAmount moneyAmount = mock(MoneyAmount.class);
        when(moneyAmount.getAmountInCents()).thenReturn((long)100);
        MoneyAmount moneyAmount2 = mock(MoneyAmount.class);
        when(moneyAmount2.getAmountInCents()).thenReturn((long)120);
        Bet bet = mock(Bet.class);
        when(bet.getMoneyAmount()).thenReturn(moneyAmount2);
        //act
        IPlayerCard receivedCard = cashier.distributeGamblerCard();
        cashier.addAmount(receivedCard, moneyAmount);
        boolean isValid = cashier.checkIfBetIsValid(null, bet);
    }
    @Test
    public void CheckBetIsValidWhenBetIsNullShouldThrowIllegalArgumentException() throws BetNotExceptedException {
        //arrange
        thrown.expect(IllegalArgumentException.class);
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        validPlayerCards.add(validPlayerCard);
        Cashier cashier = new Cashier(validPlayerCards, validBetLoggingAuthority);

        MoneyAmount moneyAmount = mock(MoneyAmount.class);
        when(moneyAmount.getAmountInCents()).thenReturn((long)100);
        //act
        IPlayerCard receivedCard = cashier.distributeGamblerCard();
        cashier.addAmount(receivedCard, moneyAmount);
        boolean isValid = cashier.checkIfBetIsValid(receivedCard, null);
    }
    @Test
    public void CheckBetIsValidWhenNotDistributedCardUsedShouldThrowIllegalArgumentException() throws BetNotExceptedException {
        //arrange
        thrown.expect(BetNotExceptedException.class);
        IPlayerCard notDistributedCard = mock(IPlayerCard.class);
        List<IPlayerCard> validPlayerCards = new ArrayList<>();
        validPlayerCards.add(validPlayerCard);
        Cashier cashier = new Cashier(validPlayerCards, validBetLoggingAuthority);

        MoneyAmount moneyAmount = mock(MoneyAmount.class);
        when(moneyAmount.getAmountInCents()).thenReturn((long)0);
        Bet bet = mock(Bet.class);
        when(bet.getMoneyAmount()).thenReturn(moneyAmount);
        //act
        boolean isValid = cashier.checkIfBetIsValid(notDistributedCard, bet);
    }
}