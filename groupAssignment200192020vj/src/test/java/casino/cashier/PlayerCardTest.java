package casino.cashier;

import casino.bet.Bet;
import casino.idfactory.BetID;
import casino.idfactory.CardID;
import casino.idfactory.ID;
import casino.idfactory.IDFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class PlayerCardTest {
    private final CardID cardIdMock = mock(CardID.class);
    private IDFactory idFactoryMock = mock(IDFactory.class);
    private PlayerCard playerCard;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void createValidPlayerCard() {
        playerCard = new PlayerCard(cardIdMock, idFactoryMock);
    }

    @Test
    public void PlayerCard_playerCardIsCreated() {
        // Arrange
        playerCard = new PlayerCard(cardIdMock, idFactoryMock);

        // Act

        // Assert
        assertThat(playerCard, instanceOf(PlayerCard.class));
    }

    @Test
    public void PlayerCard_cardIdCanNotBeNull_IllegalArgumentException() {
        // Arrange
        exception.expect(IllegalArgumentException.class);
        playerCard = new PlayerCard(null, idFactoryMock);
        // Act

        // Assert
    }

    @Test
    public void PlayerCard_idFactoryCanNotBeNull_IllegalArgumentException() {
        // Arrange
        exception.expect(IllegalArgumentException.class);
        playerCard = new PlayerCard(cardIdMock, null);
        // Act

        // Assert
    }

    @Test
    public void returnBetIDs_returnEmptySetWhenNoBetsMade() {
        // Arrange
        Set<BetID> actualBetSet;

        // Act
        actualBetSet = playerCard.returnBetIDs();

        // Assert
        assertTrue(actualBetSet.isEmpty());
    }

    @Test
    public void returnBetIDs_returnNonEmptySetEqualToBetsMade() {
        // Arrange
        final BetID EXPECTED_BET_ID_1 = mock(BetID.class);
        Set<BetID> actualBetSet;

        when(idFactoryMock.getID(ID.BET)).thenReturn(EXPECTED_BET_ID_1);

        // Act
        playerCard.generateNewBetID();
        actualBetSet = playerCard.returnBetIDs();

        int actualNumberOfBets = actualBetSet.size();

        // Assert
        assertEquals("ERROR - Expected: " + 1 + ", Actual: " + actualNumberOfBets + "\n\n",
                1, actualNumberOfBets);
    }

    @Test
    public void generateNewBetID_idShouldBeReturnedAndStoredInSetOnPlayerCard() {
        // Arrange
        final BetID EXPECTED_BET_ID = mock(BetID.class);

        when(idFactoryMock.getID(ID.BET)).thenReturn(EXPECTED_BET_ID);

        // Act
        BetID actualBetID = playerCard.generateNewBetID();
        int actualNumberOfBets = playerCard.getNumberOfBetIDs();

        // Assert
        assertThat("ERROR - Expected: " + EXPECTED_BET_ID + " Actual: " + actualBetID + "\n\n",
                actualBetID, is(EXPECTED_BET_ID));
        assertEquals("ERROR - Expected: " + 1 + ", Actual: " + actualNumberOfBets + "\n\n",
                1, actualNumberOfBets);
    }

    @Test
    public void getNumberOfBetIDs_numberShouldBeZeroWhenNoBetsGenerated() {
        // Arrange

        // Act
        int actualBetAmount = playerCard.getNumberOfBetIDs();

        // Assert
        assertEquals("ERROR - Expected 0, Actual: " + actualBetAmount + "\n\n", 0, actualBetAmount);
    }

    @Test
    public void getNumberOfBetIDs_numberShouldBeEqualToBetsGenerated() {
        // Arrange
        final BetID EXPECTED_BET_ID_1 = mock(BetID.class);
        final BetID EXPECTED_BET_ID_2 = mock(BetID.class);

        when(idFactoryMock.getID(ID.BET)).thenReturn(EXPECTED_BET_ID_1).thenReturn(EXPECTED_BET_ID_2);

        // Act
        playerCard.generateNewBetID();
        playerCard.generateNewBetID();
        int actualNumberOfBets = playerCard.getNumberOfBetIDs();

        // Assert
        assertEquals("ERROR - Expected: " + 2 + ", Actual: " + actualNumberOfBets + "\n\n",
                2, actualNumberOfBets);
    }

    @Test
    public void returnBetIDsAndClearCard_ifNoBetsMadeReturnEmptySet() {
        // Arrange
        Set<BetID> betIDSet;

        // Act
        betIDSet = playerCard.returnBetIDsAndClearCard();

        // Assert
        assertTrue("ERROR - Set returned should be empty\n\n", betIDSet.isEmpty());
    }

    @Test
    public void returnBetIDsAndClearCard_returnSetOfBetIDsAndClearBetsStoredOnCard() {
        // Arrange
        Set<BetID> expectedSet = new HashSet<>();
        Set<BetID> actualSet;
        Set<BetID> setAfterClearing;

        final BetID EXPECTED_BET_ID_1 = mock(BetID.class);
        final BetID EXPECTED_BET_ID_2 = mock(BetID.class);

        when(idFactoryMock.getID(ID.BET)).thenReturn(EXPECTED_BET_ID_1).thenReturn(EXPECTED_BET_ID_2);

        // Act
        expectedSet.add(playerCard.generateNewBetID());
        expectedSet.add(playerCard.generateNewBetID());

        actualSet = playerCard.returnBetIDsAndClearCard();
        setAfterClearing = playerCard.returnBetIDs();

        // Assert
        assertEquals("ERROR - Expected and actual sets do not match\n\n", expectedSet, actualSet);
        assertTrue("ERROR - Set on PlayerCard is not empty after clearing card", setAfterClearing.isEmpty());
    }
}